# Resources

This page contains a list of useful external resources including all the
dependencies used in this project.

## References

## Dependencies

### Build Dependencies

- [CPM](https://github.com/cpm-cmake/CPM.cmake): \emoji :package: CMake's missing
  package manager. A small CMake script for setup-free, cross-platform,
  reproducible dependency management

### Internal Dependencies

- [cxxopts](https://github.com/jarro2783/cxxopts): Lightweight C++ command line
  option parser
- [{fmt}](https://github.com/fmtlib/fmt): A modern formatting library
- [infoware](https://github.com/ThePhD/infoware): C++ Library for pulling
  system and hardware information, without hitting the command line
- [Magic Enum C++](https://github.com/Neargye/magic_enum): Static reflection
  for enums (to string, from string, iteration) for modern C++, work with any
  enum type without any macro or boilerplate code
- [Nameof C++](https://github.com/Neargye/nameof): Nameof operator for modern
  C++, simply obtain the name of a variable, type, function, macro, and enum
- [Semantic Versioning C++](https://github.com/Neargye/semver): Semantic
  Versioning for modern C++
- [spdlog](https://github.com/gabime/spdlog): Fast C++ logging library
- [TOML++](https://github.com/marzer/tomlplusplus/): Header-only TOML config
  file parser and serializer for C++17

### Testing Dependencies

- [Catch2](https://github.com/catchorg/Catch2): A modern, C++-native, test
  framework for unit-tests, TDD and BDD - using C++14, C++17 and later

### Other Dependencies

- [Doxygen Awesome](https://github.com/jothepro/doxygen-awesome-css): Custom
  CSS theme for doxygen html-documentation with lots of customization
  parameters
- [Base16](chriskempson.com/projects/base16): An architecture for building themes
- [Font Awesome](https://fontawesome.com): The iconic SVG, font, and CSS toolkit

## Attributions

- [Font Awesome](https://fontawesome.com/license): CC BY 4.0 License (https://creativecommons.org/licenses/by/4.0/)