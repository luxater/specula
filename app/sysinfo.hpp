#ifndef SPECULA_SYSINFO_HPP_
#define SPECULA_SYSINFO_HPP_

#include <string>

namespace specula::cli {
int sysinfo(std::string format);
}  // namespace specula::cli

#endif  // !SPECULA_SYSINFO_HPP_
